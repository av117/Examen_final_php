<?php
include("./conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_participante'])) {
  $id_participante= $_GET['id_participante'];
  $sql = "SELECT * FROM participantes WHERE id_participante= '$id_participante'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $edad = $row['edad'];
    $profesion= $row['profesion'];
    $email = $row['email'];
  }
}
?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Datos del participante</strong></legend>
          <div class="form-group">
            <label><strong>Nombre</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $nombre; ?>">
          </div>
          <div class="form-group">
            <label><strong>Edad</strong></label>
            <input readonly type="number" class="form-control" value="<?php echo $edad; ?>">
          </div>
          <div class="form-group">
            <label><strong>Profesion</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $profesion; ?>">
          </div>
          <div class="form-group">
            <label><strong>Correo electronico</strong></label>
            <input readonly type="email" class="form-control" value="<?php echo $email; ?>">
          </div>
        </div>

        <div class="modal-footer">
          <a href="registro_participantes.php" class="btn btn-warning">Regresar</a>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>