<?php
include("./conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_participante'])) {
  $id_participante= $_GET['id_participante'];
  $sql = "SELECT * FROM participantes WHERE id_participante= '$id_participante'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $edad = $row['edad'];
    $profesion = $row['profesion'];
    $email = $row['email'];
  }
}

if (isset($_POST['update'])) {
  $id_participante= $_GET['id_participante'];
  $nombre = $_POST['nombre'];
  $edad = $_POST['edad'];
  $profesion = $_POST['profesion'];
  $email = $_POST['email'];

  $sql = "UPDATE participantes set nombre = '$nombre', edad = '$edad', profesion = '$profesion', email = '$email' WHERE id_participante= '$id_participante'";
  mysqli_query($conn, $sql);
  
  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_participantes.php');
}

?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-5 mx-auto">
      <div class="card card-body">
        <form action="editar_participantes.php?id_participante=<?php echo $_GET['id_participante']; ?>" method="POST">
          <legend><strong>Datos del participante</strong></legend>
          <div class="form-group">
            <label><strong>Nombre</strong></label>
            <input type="text" name="nombre" class="form-control" placeholder="Nombre del panticipante" value="<?php echo $nombre; ?>" required/>
          </div>
          <div class="form-group">
            <label><strong>Edad</strong></label>
            <input type="number" name="edad" class="form-control" placeholder="Edad del panticipante" value="<?php echo $edad; ?>" required/>
          </div>
          <div class="form-group">
            <label><strong>Profesion</strong></label>
            <select name="profesion" class="form-control" required>
              <option readonly><?php echo $profesion; ?></option>
              <option>Ingeniero(a)</option>
              <option>Maestro(a)</option>
              <option>Contador(a)</option>
              <option>Doctor(a)</option>
            </select>
          </div>
          <div class="form-group">
            <label><strong>Email</strong></label>
            <input type="email" name="email" class="form-control" placeholder="Correo del participante" value="<?php echo $email; ?>" required/>
          </div>

        <a href="registro_participantes.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>